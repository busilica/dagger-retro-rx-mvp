package com.example.daca.dagger_retro_rx_mvp.mvp.presenter;

import com.example.daca.dagger_retro_rx_mvp.api.CakeApiService;
import com.example.daca.dagger_retro_rx_mvp.base.BasePresenter;
import com.example.daca.dagger_retro_rx_mvp.mapper.CakeMapper;
import com.example.daca.dagger_retro_rx_mvp.mvp.model.Cake;
import com.example.daca.dagger_retro_rx_mvp.mvp.model.CakesResponse;
import com.example.daca.dagger_retro_rx_mvp.mvp.view.MainView;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.Observer;

/**
 * Created by Daca on 23.02.2017..
 */

public class CakePresenter extends BasePresenter<MainView> implements Observer<CakesResponse> {

    @Inject protected CakeApiService apiService;
    @Inject protected CakeMapper cakeMapper;

    @Inject
    public CakePresenter() {
    }

    public void getCakes() {
        getView().onShowDialog("Loading cakes...");
        Observable<CakesResponse> cakesResponseObservable = apiService.getCakes();
        subsribe(cakesResponseObservable, this);
    }

    @Override
    public void onCompleted() {
        getView().onHideDialog();
        getView().onShowToast("Cakes loading complete!");
    }

    @Override
    public void onError(Throwable e) {
        getView().onHideDialog();
        getView().onShowToast("Error loading cakes " + e.getMessage());
    }

    @Override
    public void onNext(CakesResponse cakesResponse) {
        List<Cake> cakes = cakeMapper.mapCakes(cakesResponse);//this will give us a list of cakes
        //you can pass them one by one or all at once
        //we will pass them all at once
        getView().onCakeLoaded(cakes);
    }
}
