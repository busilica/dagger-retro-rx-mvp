package com.example.daca.dagger_retro_rx_mvp.mapper;

import com.example.daca.dagger_retro_rx_mvp.mvp.model.Cake;
import com.example.daca.dagger_retro_rx_mvp.mvp.model.CakesResponse;
import com.example.daca.dagger_retro_rx_mvp.mvp.model.CakesResponseCakes;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by Daca on 24.02.2017..
 */

public class CakeMapper {
    //CakeResponse has a lot of information, we are not interested in all of it
    //mapper turns response object into normal model object

    @Inject
    public CakeMapper() {
    }

    public List<Cake> mapCakes(CakesResponse response){
        List<Cake> cakeList = new ArrayList<>();

        if (response != null){//response might be initially empty
            CakesResponseCakes[] responseCakes = response.getCakes();
            if (responseCakes != null){
                for (CakesResponseCakes cake : responseCakes) {
                    Cake myCake = new Cake();
                    myCake.setId(cake.getId());
                    myCake.setTitle(cake.getTitle());
                    myCake.setDetailDescription(cake.getDetailDescription());
                    myCake.setPreviewDescription(cake.getPreviewDescription());
                    myCake.setImageUrl(cake.getImage());
                    cakeList.add(myCake);
                }
            }
        }
        return cakeList;
    }
}
