package com.example.daca.dagger_retro_rx_mvp.di.component;

import android.content.Context;

import com.example.daca.dagger_retro_rx_mvp.application.CakeApplication;
import com.example.daca.dagger_retro_rx_mvp.di.module.ApplicationModule;

import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Retrofit;

/**
 * Created by Daca on 23.02.2017..
 */

@Singleton  //scope
@Component(modules = ApplicationModule.class)  //type - each component should refer to a module or dependency
            //this component doesn't have a parent class - it is not dependent on other modules/components
            //it will be used as a parent for other child components
public interface ApplicationComponent {
    //here we specify where we want to specify this component
    //usually this method is called inject, but you can call it anything
    //we will call it applyInjection and we will apply it to our CakeApplication
    //void inject(CakeApplication application)
    Retrofit exposeRetrofit(); //if we want to provide Retrofit from ApplicationModule to CakeModule
                                //we have to expose it through interface first
    Context exposeContext();
}
