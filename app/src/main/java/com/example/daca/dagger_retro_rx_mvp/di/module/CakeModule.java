package com.example.daca.dagger_retro_rx_mvp.di.module;

import com.example.daca.dagger_retro_rx_mvp.api.CakeApiService;
import com.example.daca.dagger_retro_rx_mvp.di.scope.PerActivity;
import com.example.daca.dagger_retro_rx_mvp.mvp.view.MainView;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by Daca on 23.02.2017..
 */

//don't forget that we specified PER RUNTIME so every object from this modules will be PER RUNTIME
@Module
public class CakeModule {

    private MainView view;

    public CakeModule(MainView view) {
        this.view = view;
    }

    @PerActivity
    @Provides
    CakeApiService provideApiService(Retrofit retrofit){
        return retrofit.create(CakeApiService.class);
    }

    @PerActivity
    @Provides
    MainView provideView(){
        return view;
    }
}
