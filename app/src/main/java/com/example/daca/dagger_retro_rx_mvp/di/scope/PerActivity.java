package com.example.daca.dagger_retro_rx_mvp.di.scope;

/**
 * Created by Daca on 23.02.2017..
 */

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Scope determines what kind of retention element you are going to specify
 * the high scope is the singleton - which would be live through entire app lifecycle
 * we will use per activity scope, it can also be per fragment, per view, per service...
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface PerActivity {
}
