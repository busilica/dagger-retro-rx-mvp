package com.example.daca.dagger_retro_rx_mvp.di.component;

import com.example.daca.dagger_retro_rx_mvp.di.module.CakeModule;
import com.example.daca.dagger_retro_rx_mvp.di.scope.PerActivity;
import com.example.daca.dagger_retro_rx_mvp.modules.home.MainActivity;

import dagger.Component;

/**
 * Created by Daca on 23.02.2017..
 */

@PerActivity    //if you want something provided through entire lifecycle use Singleton, otherwise make and use PerActivity, View, Fragment, Service
@Component(modules = CakeModule.class, dependencies = ApplicationComponent.class) //CakeModule migh require it so we provided dependency ApplicationComponent
public interface CakeComponent {

    void inject(MainActivity activity);
}
