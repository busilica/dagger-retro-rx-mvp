package com.example.daca.dagger_retro_rx_mvp.base;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.CallSuper;
import android.support.v7.app.AppCompatActivity;

import com.example.daca.dagger_retro_rx_mvp.application.CakeApplication;
import com.example.daca.dagger_retro_rx_mvp.di.component.ApplicationComponent;

import butterknife.ButterKnife;

/**
 * Created by Daca on 23.02.2017..
 */

public abstract class BaseActivity extends AppCompatActivity {

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentView());//all layout classes will inherit this
        ButterKnife.bind(this);
        onViewReady(savedInstanceState, getIntent());
    }

    @Override
    protected void onDestroy() {
        ButterKnife.unbind(this);
        super.onDestroy();
    }

    @CallSuper  //do everything your parent does and then proceed
    protected void onViewReady(Bundle savedInstanceState, Intent intent) {
        //To be used by child activities
        resolveDaggerDependency();
    }

    protected void resolveDaggerDependency() {
    }

    protected void showDialog(String message){
        if (progressDialog == null){
            progressDialog = new ProgressDialog(this);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(true);//so it doesn't stuck the UI if something goes wrong
        }
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    protected void hideDialog(){
        if (progressDialog !=null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    protected ApplicationComponent getApplicationComponent(){
        CakeApplication application = (CakeApplication) getApplication();
        return application.getApplicationComponent();
    }

    protected abstract int getContentView();// this method will be mandatory for all classes which inherit this class
}
