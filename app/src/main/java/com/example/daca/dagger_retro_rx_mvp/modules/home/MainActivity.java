package com.example.daca.dagger_retro_rx_mvp.modules.home;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.example.daca.dagger_retro_rx_mvp.R;
import com.example.daca.dagger_retro_rx_mvp.base.BaseActivity;
import com.example.daca.dagger_retro_rx_mvp.di.component.DaggerCakeComponent;
import com.example.daca.dagger_retro_rx_mvp.di.module.CakeModule;
import com.example.daca.dagger_retro_rx_mvp.modules.home.adapter.CakeAdapter;
import com.example.daca.dagger_retro_rx_mvp.mvp.model.Cake;
import com.example.daca.dagger_retro_rx_mvp.mvp.presenter.CakePresenter;
import com.example.daca.dagger_retro_rx_mvp.mvp.view.MainView;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;

public class MainActivity extends BaseActivity implements MainView {

    @Bind(R.id.cake_list) protected RecyclerView cakeList;
    //BaseActivity can also have presenter but we will do injection just in here
    @Inject protected CakePresenter presenter;
    private CakeAdapter cakeAdapter;

    @Override
    protected void onViewReady(Bundle savedInstanceState, Intent intent) {
        super.onViewReady(savedInstanceState, intent);
        initializeList();
        presenter.getCakes();
    }

    private void initializeList() {
        cakeList.setHasFixedSize(true);
        cakeList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));//false for reversed
        cakeAdapter = new CakeAdapter(getLayoutInflater());
        cakeList.setAdapter(cakeAdapter);
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_main;
    }

    @Override
    protected void resolveDaggerDependency() {
       DaggerCakeComponent.builder()
                          .applicationComponent(getApplicationComponent())
                          .cakeModule(new CakeModule(this))
                          .build()
                          .inject(this);

    }

    @Override
    public void onCakeLoaded(List<Cake> cakes) {
        cakeAdapter.addCakes(cakes);
    }

    @Override
    public void onShowDialog(String message) {
        showDialog(message);
    }

    @Override
    public void onHideDialog() {
        hideDialog();
    }

    @Override
    public void onShowToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
