package com.example.daca.dagger_retro_rx_mvp.mvp.view;

import com.example.daca.dagger_retro_rx_mvp.mvp.model.Cake;

import java.util.List;

/**
 * Created by Daca on 23.02.2017..
 */

public interface MainView extends BaseView {
    void onCakeLoaded(List<Cake> cakes);

    void onShowDialog(String message);

    void onHideDialog();

    void onShowToast(String message);
}
