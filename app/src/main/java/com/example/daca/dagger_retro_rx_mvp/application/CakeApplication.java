package com.example.daca.dagger_retro_rx_mvp.application;

import android.app.Application;

import com.example.daca.dagger_retro_rx_mvp.di.component.ApplicationComponent;
import com.example.daca.dagger_retro_rx_mvp.di.component.DaggerApplicationComponent;
import com.example.daca.dagger_retro_rx_mvp.di.module.ApplicationModule;

/**
 * Created by Daca on 23.02.2017..
 */

public class CakeApplication extends Application {//application is a singleton object available through entire lifecycle
                                                    //here you can initialize most of the things which are required through entire lifecycle
    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initializeApplicationComponent();
    }

    private void initializeApplicationComponent() {
        applicationComponent = DaggerApplicationComponent
                .builder()
                .applicationModule(new ApplicationModule(this, "https://gist.githubusercontent.com"))
                .build();
    }

    public ApplicationComponent getApplicationComponent(){
        return applicationComponent;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
