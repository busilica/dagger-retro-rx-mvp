package com.example.daca.dagger_retro_rx_mvp.base;

import com.example.daca.dagger_retro_rx_mvp.mvp.view.BaseView;

import javax.inject.Inject;

import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Daca on 23.02.2017..
 */

public class BasePresenter<V extends BaseView> {    //V - view

    @Inject protected V view;

    protected V getView(){  //using this we can comunicate with the view - view will already be injected
        return view;
    }

    protected <T> void subsribe(Observable<T> observable, Observer<T> observer) {   //choose rx Observer, T - anyone subscribing can be generic type
        observable.subscribeOn(Schedulers.newThread())   //choose rx Schedulers
                  .toSingle()
                  .observeOn(AndroidSchedulers.mainThread())
                  .subscribe(observer);
    }
}
