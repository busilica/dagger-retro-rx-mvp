package com.example.daca.dagger_retro_rx_mvp.modules.home.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.daca.dagger_retro_rx_mvp.R;
import com.example.daca.dagger_retro_rx_mvp.mvp.model.Cake;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Daca on 24.02.2017..
 */

public class CakeAdapter extends RecyclerView.Adapter<CakeAdapter.Holder> {

    private LayoutInflater inflater;
    private List<Cake> cakeList = new ArrayList<>();

    public CakeAdapter(LayoutInflater inflater) {
        this.inflater = inflater;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.list_item_layout, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.bind(cakeList.get(position));
    }

    @Override
    public int getItemCount() {
        return cakeList.size();
    }

    public void addCakes(List<Cake> cakes){
        cakeList.addAll(cakes);
        notifyDataSetChanged();//not necessary
    }

    public class Holder extends RecyclerView.ViewHolder {

        @Bind(R.id.cake_icon) protected ImageView cakeIcon;
        @Bind(R.id.textview_title) protected TextView cakeTitle;
        @Bind(R.id.textview_preview_description) protected TextView cakePreviewDescription;

        private Context context;

        public Holder(View itemView) {
            super(itemView);
            context = itemView.getContext();
            ButterKnife.bind(this, itemView);
        }

        public void bind(Cake cake) {
            cakeTitle.setText(cake.getTitle());
            cakePreviewDescription.setText(cake.getPreviewDescription());

            Glide.with(context).load(cake.getImageUrl())
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .into(cakeIcon);
        }
    }

}
